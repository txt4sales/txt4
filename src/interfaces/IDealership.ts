import IAddress from './IAddress';

export default interface IDealership {
    codePrefix: string;
    displayName: string;
    eLeadsEmail: string;
    id: string;
    lastAssignedRepId: string;
    salesNumber: string;
    twilioNumber: string;
    vAutoDealershipId: string;
    contactName: string;
    contactAddress: IAddress;
}
