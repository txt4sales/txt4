import { ILastMessage } from '.';
import { CustomerState } from '../enums/CustomerState';
import { CustomerStatus } from '../enums/CustomerStatus';

export default interface ICustomer {
    cid: string;
    contactPref?: 'email' | 'call' | 'text';
    displayName: string;
    emailAddress?: string;
    lastAutocode?: string;
    lastMessage?: ILastMessage;
    phone: string;
    phoneClean: string;
    state: CustomerState;
    status?: CustomerStatus;
    submittedDate?: Date;
    uid: string;
}
