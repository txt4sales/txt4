export default interface IEmailContent {
    to: string;
    from?: string;
    subject: string;
    body: string;
    bodyHtml?: string;
}
