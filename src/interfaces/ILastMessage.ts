export default interface ILastMessage {
    meta?: any;
    text: string;
    timestamp: number;
    uid?: string;
}
