import * as firebase from 'firebase';

import { IFirebaseConfig } from '.';

export default interface IFirebase {
    init(config: IFirebaseConfig): Promise<void>;
    fetchCloudFunction(pathAndQuery: string): Promise<any>;
    auth(): firebase.auth.Auth;
    database(): firebase.database.Database;
}
