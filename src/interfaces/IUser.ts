export default interface IUser {
    allowAccess: boolean;
    allowNotifications: boolean;
    avatarUrl: string;
    bio: string;
    dealershipId: string;
    dealershipName: string;
    displayName: string;
    dmsid?: string;
    initials: string;
    permissions: string[];
    phone: string;
    phoneClean: string;
    role: string;
    skipRepAssignment: boolean;
    uid: string;
}
