import { IEmailContent } from '../interfaces';

export interface IMailgunConfig {
    apiKey: string;
    domain: string;
    defaultFrom: string;
}

export class Email {

    protected config: IMailgunConfig = {} as IMailgunConfig;

    public init(config: IMailgunConfig) {
        this.config = config;
    }

    public async send(email: IEmailContent): Promise<any> {
        let body = '';
        body += 'to=' + encodeURIComponent(email.to) + '&';
        body += 'from=' + encodeURIComponent(email.from || this.config.defaultFrom) + '&';
        body += 'subject=' + encodeURIComponent(email.subject) + '&';
        body += 'text=' + encodeURIComponent(email.body) + '&';
        if (email.bodyHtml) {
            body += 'html=' + encodeURIComponent(email.bodyHtml);
        }

        const authString = new Buffer('api:' + this.config.apiKey).toString('base64');
        const options = {
            method: 'POST',
            body,
            headers: {
                'Authorization': 'Basic ' + authString,
                'Content-Type': 'application/x-www-form-urlencoded',
            },
        };
        const url = `https://api.mailgun.net/v3/${this.config.domain}/messages`;

        const res = await fetch(url, options);
        const json = await res.json();
        return json;
    }
}

export default new Email();
