import { oneLine } from 'common-tags';

import { IEmailContent } from '../interfaces';
import Customers from '../repositories/Customers';
import CustomerVehicles from '../repositories/CustomerVehicles';
import Dealerships from '../repositories/Dealerships';
import Users from '../repositories/Users';
import { Adf, IAdfCustomer, IAdfVehicle, IAdfVendor } from './adf';
import email from './email';
import tracing from './tracing';

const trace = tracing.with('eleads');

export class Eleads {

    public async submitProspect(
        dealershipId: string,
        customerId: string,
        requestDate?: Date,
        emailOverride?: IEmailContent,
    ): Promise<any> {

        // get email
        let emailContent = await this.createProspectEmail(dealershipId, customerId, requestDate);
        if (!emailContent) {
            throw new Error(`Prospect email could not be created`);
        }
        // override email params
        if (emailOverride) {
            emailContent = Object.assign({}, emailContent, emailOverride);
        }

        // send email
        return email.send(emailContent);
    }

    public async createProspectEmail(
        dealershipId: string,
        customerId: string,
        requestDate?: Date,
    ): Promise<IEmailContent> {

        if (!requestDate) {
            requestDate = new Date(Date.now());
        }

        // get dealership
        const dealershipRepo = new Dealerships();
        const dealership = await dealershipRepo.get(dealershipId);
        if (!dealership) {
            throw new Error(`Dealership ${dealershipId} was not found`);
        }

        // get customer
        const customerRepo = new Customers();
        const customer = await customerRepo.get(dealershipId, customerId);
        if (!customer) {
            throw new Error(`Customer ${customerId} was not found for dealership '${dealershipId}`);
        }

        // get dmsid for sales rep
        let agentDMSID;
        if (customer.uid) {

            // get sales rep
            const userRepo = new Users();
            const user = await userRepo.get(customer.uid);
            if (user && user.dmsid) {
                agentDMSID = user.dmsid;
            }
        }

        // get customer vehicles
        const customerVehiclesRepo = new CustomerVehicles();
        const customerVehicles = await customerVehiclesRepo.get(dealershipId, customer.cid);

        // get adf body
        const emailOther = customer.emailAddress;
        const customerNumber = customer.cid.replace('+', '');
        const customerInput: IAdfCustomer = {
            id: customer.uid,
            contact: {
                name: customer.displayName,
                email: `${customer.phoneClean}@txt4sales.com`,
                emailOther,
                phone: {
                    number: customer.phone,
                    type: 'cellphone',
                },
            },
            comments: [
                oneLine`Customer engaged with Txt4Sales Easy Text,
                see conversation https://sheehy.txt4sales.com/activity/?d=${dealershipId}&c=${customerNumber}`,
            ],
        };

        const dealershipInput: IAdfVendor = {
            name: 'Sheehy Automotive',
            division: dealership.displayName,
            agentDMSID,
            contact: {
                name: dealership.contactName,
                address: {
                    street1: dealership.contactAddress.street1,
                    city: dealership.contactAddress.city,
                    regionCode: dealership.contactAddress.state,
                    postalCode: dealership.contactAddress.zipcode,
                },
            },
        };

        const vehicles: IAdfVehicle[] = [];
        if (customerVehicles && customerVehicles.vehicles) {
            for (const stockNum in customerVehicles.vehicles) {
                if (stockNum) {
                    const vehicle = customerVehicles.vehicles[stockNum];
                    if (vehicle) {
                        vehicles.push({
                            year: vehicle.year,
                            make: vehicle.make,
                            model: vehicle.model,
                            trim: vehicle.series,
                            stockNumber: stockNum,
                            vin: vehicle.vin || '',
                            isUsed: vehicle.newUsed === 'U',
                        });
                    }
                }
            }
        }

        const adf = new Adf();
        const body = adf.mergeAdfProspect(requestDate, customerInput, dealershipInput, vehicles);

        return {
            to: dealership.eLeadsEmail,
            from: 'admin@txt4sales.com',
            subject: 'Txt4Sales Easy Text Lead',
            body,
        };
    }
}

export default new Eleads();
