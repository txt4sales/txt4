import { stripIndents } from 'common-tags';

export interface IAdfAddress {
    street1: string;
    street2?: string;
    city: string;
    regionCode: string;
    postalCode: string;
}

export interface IAdfPhone {
    number: string;
    type: string;
    time?: string;
}

export interface IAdfContact {
    name: string;
    email?: string;
    emailOther?: string;
    phone?: IAdfPhone;
    address?: IAdfAddress;
    isPrimary?: boolean;
}

export interface IAdfVendor {
    name: string;
    division?: string;
    agentDMSID?: string;
    contact: IAdfContact;
}

export interface IAdfCustomer {
    id: string;
    contact: IAdfContact;
    comments?: string[];
}

export interface IAdfVehicle {
    year: string;
    make: string;
    model: string;
    trim: string;
    stockNumber: string;
    vin: string;
    isUsed: boolean;
}

export class Adf {

    public mergeAdfProspect(
        requestDate: Date,
        customer: IAdfCustomer,
        dealership: IAdfVendor,
        vehicles: IAdfVehicle[],
    ) {

        const vehiclesMerged: string = this.mergeAdfVehicles(vehicles);
        const customerMerged: string = this.mergeAdfCustomer(customer);
        const vendorMerged: string = this.mergeAdfVendor(dealership);
        const providerMerged: string = this.mergeAdfTxt4SalesProvider();

        const merged = stripIndents`
            <?xml version="1.0" encoding="UTF-8"?>
            <?adf version="1.0"?>
            <adf>
                <prospect>
                    <id source="txt4sales">${customer.id}</id>
                    <requestdate>${requestDate.toISOString()}</requestdate>
                    ${vehiclesMerged}
                    ${customerMerged}
                    ${vendorMerged}
                    ${providerMerged}
                </prospect>
            </adf>`;

        return merged.replace(/\n\s*\n/g, '\n');
    }

    protected mergeAdfVehicles(vehicles: IAdfVehicle[]): string {
        return vehicles.reverse().map(v => this.mergeAdfVehicle(v)).join('\n');
    }

    protected mergeAdfVehicle(vehicle: IAdfVehicle): string {
        const status = vehicle.isUsed === true ? 'used' : 'new';
        return stripIndents`
            <vehicle interest="buy" status="${status}">
                <year>${vehicle.year}</year>
                <make>${vehicle.make}</make>
                <model>${vehicle.model}</model>
                <trim>${vehicle.trim}</trim>
                <stock>${vehicle.stockNumber}</stock>
                <vin>${vehicle.vin}</vin>
            </vehicle>`;
    }

    protected mergeAdfCustomer(customer: IAdfCustomer): string {
        const comments = customer.comments ? `<comments>${customer.comments.join('\n')}</comments>` : '';
        const contact = this.mergeAdfContact(customer.contact);

        return stripIndents`
            <customer>
                ${contact}
                ${comments}
            </customer>
        `;
    }

    protected mergeAdfVendor(vendor: IAdfVendor): string {
        let vendorName = vendor.name;
        if (vendor.division) {
            vendorName += ` - ${vendor.division}`;
        }

        const agent = vendor.agentDMSID ? `<agent>${vendor.agentDMSID}</agent>\n` : '';
        const contact = `${this.mergeAdfContact(vendor.contact)}\n`;

        return stripIndents`
            <vendor>
                <vendorname>${vendorName}</vendorname>
                ${agent}
                ${contact}
            </vendor>`;
    }

    protected mergeAdfTxt4SalesProvider(): string {
        return this.mergeAdfProvider(
            'Txt4sales - Easy Text Lead',
            'Easy Text Lead',
            'sales@txt4sales.com',
            {
                isPrimary: true,
                name: 'Greg Buckwalter',
                email: 'bucky@txt4sales.com',
                phone: {
                    number: '717-572-5616',
                    type: 'voice',
                    time: 'day',
                },
            },
        );
    }

    protected mergeAdfProvider(
        name: string,
        service: string,
        email: string,
        contact: IAdfContact,
    ): string {
        return stripIndents`
            <provider>
                <name part="full">${name}</name>
                <service>${service}</service>
                <email>${email}</email>
                ${this.mergeAdfContact(contact)}
            </provider>`;
    }

    protected mergeAdfContact(contact: IAdfContact): string {

        const primary = contact.isPrimary === true ? ` primarycontact="1"` : '';
        const email = contact.email ? `<email preferredcontact="1">${contact.email}</email>` : '';
        const emailOther = contact.emailOther ? `<email preferredcontact="0">${contact.emailOther}</email>` : '';
        const phone = contact.phone ? `${this.mergeAdfPhone(contact.phone)}` : '';
        const address = contact.address ? `${this.mergeAdfAddress(contact.address)}` : '';

        return stripIndents`
            <contact${primary}>
                <name part="full" type="individual">${contact.name}</name>
                ${email}
                ${emailOther}
                ${phone}
                ${address}
            </contact>`;
    }

    protected mergeAdfPhone(phone: IAdfPhone): string {
        const type = phone.type ? ` type="${phone.type}"` : '';
        const time = phone.time ? ` time="${phone.time}"` : '';

        return stripIndents`
            <phone${type}${time}>${phone.number}</phone>`;
    }

    protected mergeAdfAddress(address: IAdfAddress): string {

        let streets = `<street line="1">${address.street1}</street>`;

        if (address.street2) {
            streets += `\n<street line="2">${address.street2}</street>`;
        }

        return stripIndents`
            <address>
                ${streets}
                <city>${address.city}</city>
                <regioncode>${address.regionCode}</regioncode>
                <postalcode>${address.postalCode}</postalcode>
            </address>`;
    }
}

export default new Adf();
