export {
    getEnumKeys,
    getEnumValues,
    getEnumKeyByValue,
    tryGetEnumKey,
    tryGetEnumKeyByValue,
    tryGetEnumValue,
};

function getEnumKeys(e: any): string[] {
    return Object.keys(e);
}

function getEnumValues<T extends string | number>(e: any): T[] {
    return getEnumKeys(e).map((k) => e[k]) as T[];
}

function getEnumKeyByValue<T, K extends string | number>(enumObject: T, value: K): string {
    const result = tryGetEnumKeyByValue(enumObject, value);
    if (result === undefined) {
        throw new Error(`Invalid value for 'enumObject'.`);
    }
    return result;
}

function tryGetEnumKeyByValue<T, K extends string | number>(enumObject: T, value: K): string | undefined {
    const enumObjectCopy: any = enumObject;
    for (const key of getEnumKeys(enumObjectCopy)) {
        if (enumObjectCopy[key] === value) {
            return key;
        }
    }
    return undefined;
}

function tryGetEnumValue<T, K extends string | number>(enumObject: any, key: K, defValue: T): T;
function tryGetEnumValue<T, K extends string | number>(enumObject: any, key: K, defValue?: T): T | undefined {
    const enumObjectCopy: any = enumObject;
    for (const k of getEnumKeys(enumObjectCopy)) {
        if (key === k) {
            return enumObjectCopy[key];
        }
    }
    return defValue;
}

function tryGetEnumKey<T, K extends string | number>(enumObject: any, key: K, defValue: string): K;
function tryGetEnumKey<T, K extends string | number>(enumObject: any, key: K, defValue?: string): K | undefined {
    const enumObjectCopy: any = enumObject;
    for (const k of getEnumKeys(enumObjectCopy)) {
        if (key === k) {
            return key;
        }
    }
    return tryGetEnumKeyByValue(enumObject, defValue as string) as K;
}
