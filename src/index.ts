import { CustomerState } from './enums/CustomerState';
import { CustomerStatus } from './enums/CustomerStatus';
import { ICustomer, IDealership, IFirebaseConfig, ILastMessage, IUser } from './interfaces';
import appkit, { IAppkitConfig } from './lib/appkit';
import auth from './lib/auth';
import datetime from './lib/datetime';
import eleads from './lib/eleads';
import email, { IMailgunConfig } from './lib/email';
import firebase from './lib/firebase';
import push from './lib/push';
import sms, { ITwilioConfig } from './lib/sms';
import CustomerMessages from './repositories/CustomerMessages';
import Customers from './repositories/Customers';
import CustomerVehicles from './repositories/CustomerVehicles';
import DealershipConfigs from './repositories/DealershipConfigs';
import Dealerships from './repositories/Dealerships';
import Notifications from './repositories/Notifications';
import UserNotifications from './repositories/UserNotifications';
import UserNotificationTokens from './repositories/UserNotificationTokens';
import Users from './repositories/Users';
import Vehicles from './repositories/Vehicles';

export interface IConfig {
    appkit: IAppkitConfig;
    firebase: IFirebaseConfig;
    twilio: ITwilioConfig;
    mailgun: IMailgunConfig;
}

export {
    CustomerState,
    CustomerStatus,
    ICustomer,
    IDealership,
    ILastMessage,
    IUser,
};

export class Txt4 {

    public readonly firebase = firebase;
    public readonly datetime = datetime;
    public readonly auth = auth;
    public readonly push = push;
    public readonly sms = sms;
    public readonly email = email;
    public readonly eleads = eleads;

    // repositories
    public readonly users = new Users();
    public readonly dealerships = new Dealerships();
    public readonly dealershipConfigs = new DealershipConfigs();
    public readonly notifications = new Notifications();
    public readonly userNotifications = new UserNotifications();
    public readonly userNotificationTokens = new UserNotificationTokens();
    public readonly customers = new Customers();
    public readonly customerMessages = new CustomerMessages();
    public readonly customerVehicles = new CustomerVehicles();
    public readonly vehicles = new Vehicles();

    protected txt4config: IConfig | null = null;

    public get config(): IConfig | null {
        return this.txt4config;
    }

    public init(config: IConfig) {
        firebase.init(config.firebase);
        appkit.init(config.appkit);
        sms.init(config.twilio);
        email.init(config.mailgun);
        this.txt4config = config;
    }

    public setAppkitContext(appkitContext: any) {
        appkit.setContext(appkitContext);
    }

}

export default new Txt4();
