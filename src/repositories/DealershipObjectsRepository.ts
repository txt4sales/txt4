import * as firebase from 'firebase';

import { IModelTransformer } from '../interfaces';
import FirebaseRepository, { IKeyedResult, IRecordWatch } from '../lib/firebase/FirebaseRepository';
import ModelTransformProcessor from '../lib/ModelTransformProcessor';

class DealershipObjectsRepository<T = any> {

    protected firebaseRepo = new FirebaseRepository();
    protected collection: string;
    protected keyName: string;
    protected transformer?: IModelTransformer<T>;

    constructor(collection: string, transformer?: IModelTransformer<T>, keyName: string = '') {
        this.collection = collection;
        this.transformer = transformer;
        this.keyName = keyName;
    }

    public ref(dealershipId: string) {
        return this.firebaseRepo.ref(this.formatNode(dealershipId));
    }

    public async insert(dealershipId: string, record: any, key: string = ''): Promise<T> {
        const result = await this.firebaseRepo.addRecord(this.formatNode(dealershipId), record, key, this.keyName);
        return this.transform(result);
    }

    public async update(dealershipId: string, key: string, props: any): Promise<T> {
        const result = await this.firebaseRepo.updateRecord(this.formatNode(dealershipId), key, props);
        return this.transform(result);
    }

    public updateAll(dealershipId: string, props: any): Promise<T> {
        return this.firebaseRepo.updateRecords(this.formatNode(dealershipId), props);
    }

    public async getAll(dealershipId: string): Promise<T[]> {
        const result = await this.firebaseRepo.getRecords(this.formatNode(dealershipId));
        return this.transform(result);
    }

    public getAllRaw(dealershipId: string): Promise<IKeyedResult<T>> {
        return this.firebaseRepo.getRecordsRaw(this.formatNode(dealershipId));
    }

    public async get(dealershipId: string, key: string): Promise<T> {
        const result = await this.firebaseRepo.getRecord(this.formatNode(dealershipId), key);
        return this.transform(result);
    }

    public async getAllByChildValue(
        dealershipId: string,
        child: string,
        value: any): Promise<T[]> {
        const result = await this.firebaseRepo.getRecordsByChildValue(this.formatNode(dealershipId), child, value);
        return this.transform(result);
    }

    public getAllByChildValueRaw(
        dealershipId: string,
        child: string,
        value: any): Promise<IKeyedResult> {
        return this.firebaseRepo.getRecordsByChildValueRaw(this.formatNode(dealershipId), child, value);
    }

    public watchAll(dealershipId: string, callback: (records: T[]) => void): IRecordWatch {
        return this.firebaseRepo.watchRecords(this.formatNode(dealershipId), (records: any[]) => {
            callback(this.transform(records));
        });
    }

    public watch(
        dealershipId: string,
        key: string,
        callback: (record: T) => void,
        refModifier?: (ref: firebase.database.Reference) =>
            firebase.database.Reference | firebase.database.Query,
        resModifier?: (snapshot: firebase.database.DataSnapshot) => any): IRecordWatch {
        return this.firebaseRepo.watchRecord(this.formatNode(dealershipId), key, (record: any) => {
            callback(this.transform(record));
        }, refModifier, resModifier);
    }

    public watchAllByChildValue(
        dealershipId: string,
        child: string,
        value: any,
        callback: (records: T[]) => void): IRecordWatch {
        return this.firebaseRepo.watchRecordsByChildValue(
            this.formatNode(dealershipId), child, value, (records: any[]) => {
                callback(this.transform(records));
            });
    }

    public delete(dealershipId: string, key: string): Promise<void> {
        return this.firebaseRepo.deleteRecord(this.formatNode(dealershipId), key);
    }

    public transform(data: any[], transformer?: IModelTransformer<T>): T[];
    public transform(data: any, transformer?: IModelTransformer<T>): T;
    public transform(data: any | any[], transformer?: IModelTransformer<T>): T | T[] {
        const currTransformer = this.resolveTransformer(transformer);
        if (currTransformer) {
            const processor = new ModelTransformProcessor<T>();
            return processor.transformToModel(data, currTransformer);
        }
        return data;
    }

    protected formatNode(dealershipId: string) {
        return `dealershipObjects/${dealershipId}/${this.collection}`;
    }

    protected resolveTransformer(transformer?: IModelTransformer): IModelTransformer | undefined {
        if (transformer) {
            return transformer;
        } else if (this.transformer) {
            return this.transformer;
        } else {
            return undefined;
        }
    }
}

export default DealershipObjectsRepository;
