import DealershipObjectsRepository from './DealershipObjectsRepository';

class CustomerVehicles extends DealershipObjectsRepository {

    constructor() {
        super('customerVehicles');
    }
}

export default CustomerVehicles;
