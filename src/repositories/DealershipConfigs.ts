import DealershipObjectsRepository from './DealershipObjectsRepository';

class DealershipConfigs extends DealershipObjectsRepository {

    constructor() {
        super('config');
    }
}

export default DealershipConfigs;
