import DealershipObjectsRepository from './DealershipObjectsRepository';

class Vehicles extends DealershipObjectsRepository {

    constructor() {
        super('vehicles', undefined, 'key');
    }

    public lookup(dealershipId: string, codeOrKey: string): Promise<any> {
        if (codeOrKey.indexOf('-') >= 0) {
            return this.getByCode(dealershipId, codeOrKey);
        } else {
            return this.get(dealershipId, codeOrKey);
        }
    }

    public async getByCode(dealershipId: string, code: string): Promise<any> {
        const results = await this.getAllByChildValue(dealershipId, 'code', code);
        if (results && results.length > 0) {
            return results[0];
        }
        return null;
    }
}

export default Vehicles;
