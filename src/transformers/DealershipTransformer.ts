import { IAddress, IDealership, IModelTransformer } from '../interfaces';

export default class DealershipTransformer implements IModelTransformer<IDealership> {

    public transform(data: any): IDealership {
        return {
            codePrefix: data.codePrefix,
            displayName: data.displayName,
            eLeadsEmail: data.eLeadsEmail,
            id: data.id,
            lastAssignedRepId: data.lastAssignedRepId,
            salesNumber: data.salesNumber,
            twilioNumber: data.twilioNumber,
            vAutoDealershipId: data.vAutoDealershipId,
            contactName: data.contactName,
            contactAddress: this.transformContactAddress(data.contactAddress),
        };
    }

    protected transformContactAddress(data: any): IAddress {
        return {
            street1: data && data.street1,
            street2: data && data.street2,
            city: data && data.city,
            state: data && data.state,
            zipcode: data && data.zipcode,
        };
    }
}
