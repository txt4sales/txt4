import { IModelTransformer, IUser } from '../interfaces';

export default class UserTransformer implements IModelTransformer<IUser> {

    public transform(data: any): IUser {
        return {
            allowAccess: data.allowAccess === true,
            allowNotifications: data.allowNotifications === true,
            avatarUrl: data.avatarUrl || '',
            bio: data.bio || '',
            dealershipId: data.dealershipId || '',
            dealershipName: data.dealershipName || '',
            displayName: data.displayName || '',
            dmsid: data.dmsid,
            initials: data.initials || '',
            permissions: data.permissions || '',
            phone: data.phone || '',
            phoneClean: data.phoneClean || '',
            role: data.role || '',
            skipRepAssignment: data.skipRepAssignment === true,
            uid: data.uid || '',
        };
    }
}
