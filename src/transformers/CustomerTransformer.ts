import { CustomerState } from '../enums/CustomerState';
import { CustomerStatus } from '../enums/CustomerStatus';
import { ICustomer, ILastMessage, IModelTransformer } from '../interfaces';
import { tryGetEnumKey } from '../lib/enums';
import phones from '../lib/phones';

export default class CustomerTransformer implements IModelTransformer<ICustomer> {

    public transform(data: any): ICustomer {
        return {
            cid: data.cid || '',
            contactPref: data.contactPref,
            displayName: data.displayName || '',
            emailAddress: data.emailAddress,
            lastAutocode: data.lastAutocode,
            lastMessage: this.transformLastMessage(data.lastMessage),
            phone: phones.format(data.cid),
            phoneClean: phones.clean(data.cid),
            state: tryGetEnumKey(CustomerState, data.state, CustomerState.other),
            status: tryGetEnumKey(CustomerStatus, data.status, CustomerStatus.other),
            submittedDate: data.submittedDate && new Date(data.submittedDate),
            uid: data.uid || '',
        };
    }

    protected transformLastMessage(lastMessage?: ILastMessage): ILastMessage | undefined {
        if (!lastMessage) {
            return undefined;
        }

        return {
            meta: lastMessage.meta,
            text: lastMessage.text || '',
            timestamp: lastMessage.timestamp || 0,
            uid: lastMessage.uid,
        };
    }
}
