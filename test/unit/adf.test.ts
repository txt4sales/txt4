import 'mocha';

import { expect } from 'chai';
import { stripIndents } from 'common-tags';

import { Adf, IAdfCustomer, IAdfVehicle, IAdfVendor } from '../../src/lib/adf';

describe('lib/adf.ts', () => {

    it('should create adf prospect xml', async () => {

        const requestDate = new Date();

        const customer: IAdfCustomer = {
            id: '-LSpAdP9sXb9IH-H3Kfv',
            contact: {
                name: 'Bill Johnson',
                phone: {
                    number: '717-298-3774',
                    type: 'cellphone',
                },
            },
            comments: ['Customer engaged with Txt4Sales Easy Text'],
        };

        const dealership: IAdfVendor = {
            name: 'Sheehy Automotive',
            division: 'Sheehy Volkswagen of Springfield',
            agentDMSID: '256914',
            contact: {
                name: 'Scott O\'Brien',
                address: {
                    street1: '6601 Backlick Rd',
                    city: 'Springfield',
                    regionCode: 'VA',
                    postalCode: '22150',
                },
            },
        };

        const vehicle1: IAdfVehicle = {
            year: '2010',
            make: 'Ford',
            model: 'Edge',
            trim: 'First Query',
            stockNumber: 'P72810',
            vin: '1234ASD9876SFF',
            isUsed: true,
        };

        const vehicle2: IAdfVehicle = {
            year: '2017',
            make: 'Honda',
            model: 'Accord',
            trim: 'Second Query',
            stockNumber: 'A7264',
            vin: '2347569247858726',
            isUsed: false,
        };

        const adf = new Adf();
        const result = adf.mergeAdfProspect(requestDate, customer, dealership, [vehicle1, vehicle2]);

        expect(result).to.equal(stripIndents`
            <?xml version="1.0" encoding="UTF-8"?>
            <?adf version="1.0"?>
            <adf>
                <prospect>
                    <id source="txt4sales">-LSpAdP9sXb9IH-H3Kfv</id>
                    <requestdate>${requestDate.toISOString()}</requestdate>
                    <vehicle interest="buy" status="new">
                        <year>2017</year>
                        <make>Honda</make>
                        <model>Accord</model>
                        <trim>Second Query</trim>
                        <stock>A7264</stock>
                        <vin>2347569247858726</vin>
                    </vehicle>
                    <vehicle interest="buy" status="used">
                        <year>2010</year>
                        <make>Ford</make>
                        <model>Edge</model>
                        <trim>First Query</trim>
                        <stock>P72810</stock>
                        <vin>1234ASD9876SFF</vin>
                    </vehicle>
                    <customer>
                        <contact>
                            <name part="full" type="individual">Bill Johnson</name>
                            <phone type="cellphone">717-298-3774</phone>
                        </contact>
                        <comments>Customer engaged with Txt4Sales Easy Text</comments>
                    </customer>
                    <vendor>
                        <vendorname>Sheehy Automotive - Sheehy Volkswagen of Springfield</vendorname>
                        <agent>256914</agent>
                        <contact>
                            <name part="full" type="individual">Scott O'Brien</name>
                            <address>
                                <street line="1">6601 Backlick Rd</street>
                                <city>Springfield</city>
                                <regioncode>VA</regioncode>
                                <postalcode>22150</postalcode>
                            </address>
                        </contact>
                    </vendor>
                    <provider>
                        <name part="full">Txt4sales - Easy Text Lead</name>
                        <service>Easy Text Lead</service>
                        <email>sales@txt4sales.com</email>
                        <contact primarycontact="1">
                            <name part="full" type="individual">Greg Buckwalter</name>
                            <email preferredcontact="1">bucky@txt4sales.com</email>
                            <phone type="voice" time="day">717-572-5616</phone>
                        </contact>
                    </provider>
                </prospect>
            </adf>`);
    });

});
