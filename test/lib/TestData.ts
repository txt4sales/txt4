// tslint:disable:max-line-length
const testData = {
    seeded: {
        dealerships: {
            'sheehy-subaru-of-springfield': {
                codePrefix: 'SS',
                displayName: 'Sheehy Subaru of Springfield',
                id: 'sheehy-subaru-of-springfield',
                lastAssignedRepId: '-LQnz0k000MrL6-pSQiV',
                salesNumber: '703-712-8700',
                twilioNumber: '+17039918718',
                vAutoDealershipId: 'MP7105',
                eLeadsEmail: 'sheehysubaru@eleadtrack.net',
                contactName: 'Scott O\'Brien',
                contactAddress: {
                    street1: '6601 Backlick Rd',
                    street2: 'Suite 100',
                    city: 'Springfield',
                    state: 'VA',
                    zipcode: '22150',
                },
            },
        },
        dealershipObjects: {
            'sheehy-subaru-of-springfield': {
                customers: {
                    '+17172993546': {
                        cid: '+17172993546',
                        contactPref: 'email',
                        displayName: '',
                        emailAddress: 'jay@here.com',
                        lastAutocode: 'AAA-555',
                        lastMessage: {
                            text: 'Here\'s the information for AAA-555:\n2015 Champagne Silver Metallic Chevrolet LT with DARK TITANIUM/JET BLACK interior\n Mileage:20,215\n KBB Price: $17,490\n Our Price:$17,490\nWarranty: With our exclusive 6 month/Unlimited Miles warranty, you\'ll have peace of mind knowing that you\'ve selected a quality vehicle. ',
                            timestamp: 1535664571903,
                            uid: '0-txt4-bot-id',
                            meta: {
                                foo: 'bar',
                            },
                        },
                        state: 'inactive',
                        status: 'nointerest',
                        submittedDate: 1535664571903,
                        uid: '-LL0R5_C8D503GowuLj1',
                    },
                },
                customerVehicles: {
                    '+17172993546': {
                        avgPrice: 21812,
                        lastVehicle: 'R12195',
                        queryCount: 10,
                        vehicles: {
                            R12044A: {
                                make: 'INFINITI',
                                model: 'Q50',
                                newUsed: 'U',
                                price: 22941,
                                queryCount: 4,
                                series: '2.0t Premium',
                                timestamp: 1541680969549,
                                year: '2016',
                            },
                            R12195: {
                                make: 'INFINITI',
                                model: 'QX50',
                                newUsed: 'N',
                                price: 0,
                                queryCount: 1,
                                series: 'ESSENTIAL',
                                timestamp: 1541721913296,
                                year: '2019',
                            },
                            R12198A: {
                                make: 'INFINITI',
                                model: 'Q60',
                                newUsed: 'U',
                                price: 24994,
                                queryCount: 4,
                                series: 'Base',
                                timestamp: 1541681033575,
                                year: '2015',
                            },
                            R12371B: {
                                make: 'Lexus',
                                model: 'ES',
                                newUsed: 'U',
                                price: 17502,
                                queryCount: 1,
                                series: '350',
                                timestamp: 1541681080468,
                                year: '2013',
                            },
                        },
                    },
                },
            },
        },
        users: {
            '-LTDIDIwyplfhSzZDUFZ': {
                allowAccess: true,
                allowNotifications: true,
                avatarUrl: '',
                bio: 'Top salesman!',
                dealershipId: 'sheehy-subaru-of-springfield',
                dealershipName: 'Sheehy Subaru of Springfield',
                displayName: 'Darin Wilson',
                dmsid: 'DM2635',
                initials: 'DW',
                permissions: '',
                phone: '(545) 454-5454',
                phoneClean: '5454545454',
                role: 'rep',
                skipRepAssignment: false,
                uid: '-LTDIDIwyplfhSzZDUFZ',
            },
        },
    },
    transformed: {
        customers: {
            '+17172993546': {
                cid: '+17172993546',
                contactPref: 'email',
                displayName: '',
                emailAddress: 'jay@here.com',
                lastAutocode: 'AAA-555',
                lastMessage: {
                    text: 'Here\'s the information for AAA-555:\n2015 Champagne Silver Metallic Chevrolet LT with DARK TITANIUM/JET BLACK interior\n Mileage:20,215\n KBB Price: $17,490\n Our Price:$17,490\nWarranty: With our exclusive 6 month/Unlimited Miles warranty, you\'ll have peace of mind knowing that you\'ve selected a quality vehicle. ',
                    timestamp: 1535664571903,
                    uid: '0-txt4-bot-id',
                    meta: {
                        foo: 'bar',
                    },
                },
                state: 'inactive',
                status: 'nointerest',
                submittedDate: new Date(1535664571903),
                uid: '-LL0R5_C8D503GowuLj1',
                phone: '(717) 299-3546',
                phoneClean: '7172993546',
            },
        },
    },
};

export default testData;
