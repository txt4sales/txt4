import FirebaseServer = require('firebase-server');

import testData from './TestData';

class FirebaseTestServer {

    public initialized: boolean = false;

    public init() {
        if (!this.initialized) {
            this.initialized = true;
            new FirebaseServer(5000, 'localhost', testData.seeded as any);
        }
    }
}

export default new FirebaseTestServer();
