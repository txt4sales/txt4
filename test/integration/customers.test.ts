import 'mocha';

import { expect } from 'chai';

import config from '../../config';
import Txt4 from '../../src';
import FirebaseTestServer from '../lib/FirebaseTestServer';
import testData from '../lib/TestData';

before(() => {
    FirebaseTestServer.init();
});

describe('models/Customers.ts', () => {

    it('should get customer', async () => {

        await Txt4.init(config);

        const customer = testData.transformed.customers['+17172993546'];

        const result = await Txt4.customers.get('sheehy-subaru-of-springfield', '+17172993546');
        expect(result).to.deep.equal(customer);
    });

});
