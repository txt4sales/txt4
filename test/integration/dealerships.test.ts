import 'mocha';

import { expect } from 'chai';

import config from '../../config';
import Txt4 from '../../src';
import FirebaseTestServer from '../lib/FirebaseTestServer';
import testData from '../lib/TestData';

before(() => {
    FirebaseTestServer.init();
});

describe('models/Dealerships.ts', () => {

    it('should get dealership', async () => {

        await Txt4.init(config);

        const getByResult = await Txt4.dealerships.getByTwilioNumber('+17039918718');
        expect(getByResult).to.deep.equal(testData.seeded.dealerships['sheehy-subaru-of-springfield']);
    });

});
