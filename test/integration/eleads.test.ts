// tslint:disable:max-line-length
import 'mocha';

import { expect } from 'chai';
import { stripIndents } from 'common-tags';

import config from '../../config';
import Txt4 from '../../src';
import { IEmailContent } from '../../src/interfaces';
import FirebaseTestServer from '../lib/FirebaseTestServer';

before(() => {
    FirebaseTestServer.init();
});

describe('lib/eleads.ts', () => {

    it.skip('should submit prospect', async () => {

        await Txt4.init(config);

        const override: IEmailContent = {
            to: 'dave@markkup.com',
        } as IEmailContent;

        const result = await Txt4.eleads.submitProspect(
            'sheehy-subaru-of-springfield',
            '+17172993546',
            undefined,
            override,
        );
    });

    it('should create prospect email', async () => {

        await Txt4.init(config);

        const requestDate = new Date();
        const to = 'sheehysubaru@eleadtrack.net';
        const from = 'admin@txt4sales.com';
        const subject = 'Txt4Sales Easy Text Lead';
        const body = stripIndents`<?xml version="1.0" encoding="UTF-8"?>
            <?adf version="1.0"?>
            <adf>
                <prospect>
                    <id source="txt4sales">-LL0R5_C8D503GowuLj1</id>
                    <requestdate>${requestDate.toISOString()}</requestdate>
                    <vehicle interest="buy" status="used">
                        <year>2013</year>
                        <make>Lexus</make>
                        <model>ES</model>
                        <trim>350</trim>
                        <stock>R12371B</stock>
                        <vin></vin>
                    </vehicle>
                    <vehicle interest="buy" status="used">
                        <year>2015</year>
                        <make>INFINITI</make>
                        <model>Q60</model>
                        <trim>Base</trim>
                        <stock>R12198A</stock>
                        <vin></vin>
                    </vehicle>
                    <vehicle interest="buy" status="new">
                        <year>2019</year>
                        <make>INFINITI</make>
                        <model>QX50</model>
                        <trim>ESSENTIAL</trim>
                        <stock>R12195</stock>
                        <vin></vin>
                    </vehicle>
                    <vehicle interest="buy" status="used">
                        <year>2016</year>
                        <make>INFINITI</make>
                        <model>Q50</model>
                        <trim>2.0t Premium</trim>
                        <stock>R12044A</stock>
                        <vin></vin>
                    </vehicle>
                    <customer>
                        <contact>
                            <name part="full" type="individual"></name>
                            <email preferredcontact="1">7172993546@txt4sales.com</email>
                            <email preferredcontact="0">jay@here.com</email>
                            <phone type="cellphone">(717) 299-3546</phone>
                        </contact>
                        <comments>Customer engaged with Txt4Sales Easy Text, see conversation https://sheehy.txt4sales.com/activity/?d=sheehy-subaru-of-springfield&c=17172993546</comments>
                    </customer>
                    <vendor>
                        <vendorname>Sheehy Automotive - Sheehy Subaru of Springfield</vendorname>
                        <contact>
                            <name part="full" type="individual">Scott O'Brien</name>
                            <address>
                                <street line="1">6601 Backlick Rd</street>
                                <city>Springfield</city>
                                <regioncode>VA</regioncode>
                                <postalcode>22150</postalcode>
                            </address>
                        </contact>
                    </vendor>
                    <provider>
                        <name part="full">Txt4sales - Easy Text Lead</name>
                        <service>Easy Text Lead</service>
                        <email>sales@txt4sales.com</email>
                        <contact primarycontact="1">
                            <name part="full" type="individual">Greg Buckwalter</name>
                            <email preferredcontact="1">bucky@txt4sales.com</email>
                            <phone type="voice" time="day">717-572-5616</phone>
                        </contact>
                    </provider>
                </prospect>
            </adf>`;

        const result = await Txt4.eleads.createProspectEmail(
            'sheehy-subaru-of-springfield',
            '+17172993546',
            requestDate,
        );
        expect(result).to.deep.equal({
            body,
            to,
            from,
            subject,
        } as IEmailContent);
    });

});
