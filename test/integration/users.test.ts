import 'mocha';

import { expect } from 'chai';

import config from '../../config';
import Txt4, { IUser } from '../../src';
import FirebaseTestServer from '../lib/FirebaseTestServer';
import testData from '../lib/TestData';

before(() => {
    FirebaseTestServer.init();
});

describe('models/Users.js', () => {

    it('should get user', async () => {

        await Txt4.init(config);

        const getByResult = await Txt4.users.get('-LTDIDIwyplfhSzZDUFZ');
        expect(getByResult).to.deep.equal(testData.seeded.users['-LTDIDIwyplfhSzZDUFZ']);
    });

    it('should get user vie phone', async () => {

        await Txt4.init(config);

        const getByResult = await Txt4.users.getByPhone('5454545454');
        expect(getByResult).to.deep.equal(testData.seeded.users['-LTDIDIwyplfhSzZDUFZ']);
    });

    it('should add and manipulate user', async () => {

        await Txt4.init(config);

        // add user
        const addResult = await Txt4.users.add('John Test', 'Salesperson', '(717) 219-7355');
        const getResult = await Txt4.users.get(addResult.uid);
        expect(getResult.bio).to.equal('Salesperson');

        // update user
        const updateResult = await Txt4.users.update(getResult.uid, { bio: 'Manager' });
        expect(updateResult.bio).to.equal('Manager');

        // get by phoneClean
        const getByResult = await Txt4.users.getByPhone('7172197355') as IUser;
        expect(getByResult.phoneClean).to.equal('7172197355');

        // delete user
        await Txt4.users.delete(getResult.uid);
        const getDeletedResult = await Txt4.users.get(getResult.uid);
        expect(getDeletedResult).to.be.null;
    });

    it('should get empty array when getByPhone finds nothing', async () => {

        await Txt4.init(config);

        const getByResult = await Txt4.users.getByPhone('7172846574');
        expect(getByResult).to.be.null;
    });

});
